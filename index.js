var express = require('express');
var app = express();
var swig = require('swig');

var bodyParser = require('body-parser');
app.use(bodyParser.json());       // to support JSON-encoded bodies
/*app.use(bodyParser.urlencoded({     // to support URL-encoded bodies
 extended: false
 }));*/

app.set('port', (process.env.PORT || 80));
app.use('/', express.static(__dirname + '/public'));

// Handling POSTs from the client side form
app.post('/', function (request, response) {
    var temp = request.body.temp;
    var unit = request.body.unit;

    console.log(JSON.stringify(request.body));

    switch (unit) {
        case "C":
            var result = temp * (9 / 5) + 32;
            result = Math.round(result * 100) / 100  + " °F";
            break;
        case "F":
            var result = ( temp - 32 ) * (5 / 9);
            result = Math.round(result * 100) / 100 + " °C";
            break;
    }

    response.send(result);
});

// Server Listener
app.listen(app.get('port'), function () {
    console.log('Node app is running on port', app.get('port'));
});


// This is where all the magic happens!
app.engine('html', swig.renderFile);

app.set('view engine', 'html');
app.set('views', __dirname + '/public/templates');

// Swig will cache templates for you, but you can disable
// that and use Express's caching instead, if you like:
app.set('view cache', false);
// To disable Swig's cache, do the following:
swig.setDefaults({ cache: false });
// NOTE: You should always cache templates in a production environment.
// Don't leave both of these to `false` in production!

app.get('/temp-convert', function (req, res) {
    var temp = req.query.temp;
    var unit = req.query.unit;

    switch (unit) {
        case "C":
            var result = temp * (9 / 5) + 32;
            result = Math.round(result * 100) / 100  + " °F";
            break;
        case "F":
            var result = ( temp - 32 ) * (5 / 9);
            result = Math.round(result * 100) / 100 + " °C";
            break;
    }

    res.render('temp-converter', { convertRes: result });
});

app.get('/fahrenheit', function(request, response) {
    var celsius = request.query.celsius;
    if(celsius) {
        var fahrenheit = 1.8 * celsius + 32;
        response.send(celsius + ' &deg;C = ' + fahrenheit + ' &deg;F');
    }
});
