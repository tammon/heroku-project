/**
 * Created by tammschw on 27/05/15.
 */
$(document).ready(function () {
    $('#btn-convert').click(convert);

    $('form').submit(function (e) {
        e.preventDefault();
    });

    $('nav').load('/navbar.html #ajax-nav');
});


function convert () {
    if ($('#temp').val()) {

        var parse = {};
        parse.unit = $('#unit').val();
        parse.temp = $('#temp').val();

        $.ajax({
            url: 'http://node.tammoserver.com',
            type: 'POST',
            contentType: 'application/json',
            data: JSON.stringify(parse),
            success: function (data) {
                $('#result').text(data);
            },
            error: function (xhr, status, error) {
                console.log('Error: ' + error.message);
                $('#result').text('Error connecting to the server.');
            }
        });
    }
}

function ajaxconvert() {
    var celsius = document.getElementById("input").value;
    $.get("/fahrenheit?celsius=" + celsius, function(data, status) {
        document.getElementById("result").innerHTML = data;
    });
}
